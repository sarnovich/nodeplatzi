const express = require('express');
const bodyParser = require('body-parser')
const router = express.Router()
const response = require('./modulos/response')


const app = express();
app.use(bodyParser.json()) //es el content type
app.use(bodyParser.urlencoded({extended: false}));

app.use(router)

router.get('/', function(req, res) {
   
  response.success(req, res, "que llega de status?")
})

router.post('/', function(req, res) {
    let data = req.body
   
   
    res.json(`un post ok ${data}`)
})
app.use('/app', express.static('public'));
app.listen(3500);