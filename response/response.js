const responseType = {}

responseType.success = (req, res, message, status) => {
    res.status(status || 200).send({
        "error": "''",
        "body": message
    });
}
responseType.created = (req, res) => {
    res.status(201).json('llego un 201')
}
responseType.error = (req, res, message, status) => {
    res.status(status || 501).send({
        "error": message,
        "body": "''"
    });
}
module.exports = responseType