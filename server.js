const express = require('express');
const bodyParser = require('body-parser')
const app = express();
app.use(bodyParser.json()) //es el content type
//app.use(bodyParser.urlencoded({extended: false}));
//const router = require('./components/message/network')
const router = require('./network/routes')



router(app);

app.use('/app', express.static('public'));
app.listen(3500);