const express = require('express');
const responseType = require('../../response/response');
const controller = require('./controller')
const router = express.Router();

router.get('/', function (req, res) {
   
  controller.getMessage()
  .then((messageList) => {
    responseType.success(req, res, messageList, 200)
  })
  .catch (error => {
    responseType.error(req, res, "error fuerte", 500)
  })
  //responseType.success(req, res, "get complete", 233)
    
});

router.post('/', function (req, res) {
  
  let data = req.body
  //console.log(data)
  controller.addMessage(data.user, data.message)
  .then((fullMessage) => {
    responseType.success(req, res, fullMessage, 201)
  })
    .catch (error => {
      responseType.error(req, res, "info invalida", 400)
    })
})

module.exports = router