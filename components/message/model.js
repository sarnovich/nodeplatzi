const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const mySchema = new Schema({
    "user": String,
    "message": {
        "type": String,
        "required": true
    },
    "date": Date
});

const model = mongoose.model('Message', mySchema); // Para usar el schema creamos el modelo que se guardara con el nombre message la coleccion

module.exports = model;
